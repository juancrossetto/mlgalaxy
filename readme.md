##La APP se realizo en .NET Core 2.2, con C# y una base de datos en Memoria.
La misma se hosteo con Google Cloud.

Para consumir los API Rest, se utilizo swagger, a los mismos se pueden acceder a traves de la siguiente URL:
 https://meli-galaxy.appspot.com/swagger/index.html

Para monitorear el proceso batch y tambien poder ejecutarlo manualmente, se accede a traves de la siguiente URL:
https://meli-galaxy.appspot.com/process/




##ACLARACION:
-La aplicacion se inicia con datos precargados (3 planetas llamados Ferengi, Betasoide y Vulcano, y 1 sistema solar llamado MELI),
para agilizar las pruebas.
-El proceso para calcular el pronostico de un sistema solar se puede correr desde dos lugares diferentes:
 		- Por la url de process en la cual nose le indican parametros.
		- Por la url de swagger en el apartado de Forecast existen 2 maneras:
 				- api/Forecast/GenerateForecast : Se le indica la cantidad de dias y el nombre del sistema solar al cual se le va a calcular el tiempo.
 				- api/Forecast/GenerateBackgroundForecast: se le indica una cantidad de dias
 En caso de no indicarse la cantidad de dias en cualquiera de las 3 maneras de correr el proceso, se calcula con parametros del appsettings.json (3650 dias default).

Se realizaron algunos test de dominio para poder probar la logica con NUnit.
