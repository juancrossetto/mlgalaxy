﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Test.Builders;
using System;
using Xunit;

namespace Galaxy.API.Test.Test.Domain
{
    public class PlanetTest
    {

        [Fact]
        public void Positions_Test()
        {
            Planet p1 = new PlanetBuilder().WithSpeed(1)
                    .WithName("Test Position")
                    .WithDistance(2000).Build();
            Coordinates position = p1.GetPosition(10);
            Coordinates expected = new Coordinates(1969.61550602, 347.29635533);
            Assert.Equal(position.X, expected.X);
            Assert.Equal(position.Y, expected.Y);

            position = p1.GetPosition(90);
            expected = new Coordinates(0, 2000);
            Assert.Equal(position.X, expected.X);
            Assert.Equal(position.Y, expected.Y);

            position = p1.GetPosition(180);
            expected = new Coordinates(-2000, 0);
            Assert.Equal(position.X, expected.X);
            Assert.Equal(position.Y, expected.Y);

            position = p1.GetPosition(360);
            expected = new Coordinates(2000, 0);
            Assert.Equal(position.X, expected.X);
            Assert.Equal(position.Y, expected.Y);

            Planet p2 = new PlanetBuilder().WithSpeed(-5)
                 .WithName("Test Position With negative speed")
                 .WithDistance(500).Build();

            position = p2.GetPosition(40);
            expected = new Coordinates(-469.84631039, 171.01007166);
            Assert.Equal(position.X, expected.X);
            Assert.Equal(position.Y, expected.Y);
        }
        [Fact]
        public void Distance_Test()
        {
            Planet p1 = new PlanetBuilder().WithSpeed(9).WithDistance(1000).Build();
            Planet p2 = new PlanetBuilder().WithSpeed(9).WithDistance(1000).Build();
            double distanceTwoPlanets = GeometryUtils.GetDistanceBwPoints(p1.GetPosition(0), p2.GetPosition(10));
            double distExpected = GeometryUtils.GetDistanceBwPoints(new Coordinates(0, 1000), new Coordinates(1000, 0));
            Assert.Equal(distanceTwoPlanets, distExpected);
        }

        [Fact]
        public void Planet_Rotation_With_Negative_Speed_Test()
        {
            int speed = -1;
            int distance = 1;
            Planet planet = new PlanetBuilder().WithSpeed(speed)
                            .WithName("Test Negative Speed")
                            .WithDistance(distance).Build();
            int day = 10;
            Assert.Equal(GeometryUtils.GetAngleInDegrees(day, planet.Speed), Convert.ToDouble(350));

            day = 180;
            Assert.Equal(GeometryUtils.GetAngleInDegrees(day, planet.Speed), Convert.ToDouble(180));

            day = 210;
            Assert.Equal(GeometryUtils.GetAngleInDegrees(day, planet.Speed), Convert.ToDouble(150));

            day = 440;
            Assert.Equal(GeometryUtils.GetAngleInDegrees(day, planet.Speed), Convert.ToDouble(280));
        }

        [Fact]
        public void PlanetsAreNotAligned_Test()
        {
            int days = 10;
            Planet p1 = new PlanetBuilder().WithVulcanoValues().Build();
            Planet p2 = new PlanetBuilder().WithFerengiValues().Build();
            Planet p3 = new PlanetBuilder().WithBetasoideValues().Build();
            Assert.False(GeometryUtils.PointsAligned(p1.GetPosition(days), p2.GetPosition(days), p3.GetPosition(days)));

        }

        [Fact]
        public void Planets_Form_A_Triangle_Test()
        {
            int days = 15;
            Planet p1 = new PlanetBuilder().WithVulcanoValues().Build();
            Planet p2 = new PlanetBuilder().WithFerengiValues().Build();
            Planet p3 = new PlanetBuilder().WithBetasoideValues().Build();
            Assert.True(GeometryUtils.IsTriangle(p1.GetPosition(days), p2.GetPosition(days), p3.GetPosition(days)));
        }

        //[Fact]
        //public void Is_A_Point_Inside_Of_Triangle_Test()
        //{
        //    int days = 15;
        //    Planet p1 = new PlanetBuilder().WithVulcanoValues().Build();
        //    Planet p2 = new PlanetBuilder().WithFerengiValues().Build();
        //    Planet p3 = new PlanetBuilder().WithBetasoideValues().Build();
        //    Coordinates sunCoordinates = new Coordinates();
        //    Assert.True(GeometryUtils.PointIsInsideOfTriangle(p1.GetPosition(days), p2.GetPosition(days), p3.GetPosition(days), sunCoordinates));
        //}

     

    }
}
