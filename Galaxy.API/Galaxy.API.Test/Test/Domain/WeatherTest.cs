﻿using Galaxy.API.Controllers;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Process;
using Galaxy.API.Core.Services;
using Galaxy.API.Core.Services.Communication;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Infrastructure.ServicesImpl;
using Galaxy.API.Test.Builders;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Galaxy.API.Test.Test.Domain
{
    public class WeatherTest
    {

        [Fact]
        public void Is_Rainy_Day_Test()
        {
            Planet p1 = new PlanetBuilder().WithName("Planet one").WithDistance(100).WithSpeed(1).Build();
            Planet p2 = new PlanetBuilder().WithName("Planet two").WithDistance(300).WithSpeed(2).Build();
            Planet p3 = new PlanetBuilder().WithName("Planet three").WithDistance(500).WithSpeed(-5).Build();
            Coordinates sunPosition = new Coordinates();
            Coordinates positionP1 = p1.GetPosition(45);
            Coordinates positionP2 = p2.GetPosition(70);
            Coordinates positionP3 = p3.GetPosition(10);
            bool isRainyDay = GeometryUtils.PointIsInsideOfTriangle(positionP1, positionP2, positionP3, sunPosition);
            Assert.True(isRainyDay);
        }

        [Fact]
        public void Is_Drought_Day_Test()
        {
            Planet p1 = new PlanetBuilder().WithName("Planet one").WithDistance(1).WithSpeed(1).Build();
            Planet p2 = new PlanetBuilder().WithName("Planet two").WithDistance(1).WithSpeed(2).Build();
            Planet p3 = new PlanetBuilder().WithName("Planet three").WithDistance(1).WithSpeed(-5).Build();
            Coordinates sunPosition = new Coordinates();
            Coordinates positionP1 = p1.GetPosition(90);
            Coordinates positionP2 = p2.GetPosition(45);
            Coordinates positionP3 = p3.GetPosition(18);
            bool isDroughtDay = (GeometryUtils.PointsAligned(positionP1, positionP2, positionP3)
                                && GeometryUtils.PointsAligned(positionP1, positionP2, sunPosition));
            Assert.True(isDroughtDay);
        }

        [Fact]
        public void Is_Normal_Day_Test()
       {
            int days = 12;
            Planet p1 = new PlanetBuilder().WithVulcanoValues().Build();
            Planet p2 = new PlanetBuilder().WithFerengiValues().Build();
            Planet p3 = new PlanetBuilder().WithBetasoideValues().Build();
            Coordinates sunPosition = new Coordinates();
            Coordinates positionP1 = p1.GetPosition(days);
            Coordinates positionP2 = p2.GetPosition(days);
            Coordinates positionP3 = p3.GetPosition(days);
            bool isNormalDay = 
                ((!GeometryUtils.PointsAligned(positionP1, positionP2, positionP3) && GeometryUtils.IsTriangle(positionP1, positionP2, positionP3))
             || (GeometryUtils.IsTriangle(positionP1, positionP2, positionP3) && !GeometryUtils.PointIsInsideOfTriangle(positionP1, positionP2, positionP3, sunPosition)));
            Assert.True(isNormalDay);

        }
    }
}
