﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Shared.Utils;
using System;
using Xunit;

namespace Galaxy.API.Test.Test.Domain
{

    public class GeometryTest
    {

        [Fact]
        public void Calculate_Distance_Between_Two_Points_Test()
        {
            int distance = 5;
            Coordinates c1 = new Coordinates(distance, 0);
            Coordinates c2 = new Coordinates(distance * -1, 0);
            double expected = distance * 2;
            double result = GeometryUtils.GetDistanceBwPoints(c1, c2);
            Assert.Equal(result, expected);
        }

        [Fact]
        public void Form_Is_Triangle_Test()
        {
            Coordinates point1 = new Coordinates(0.0, 0.0);
            Coordinates point2 = new Coordinates(10.0, 0.0);
            Coordinates point3 = new Coordinates(0.0, 10.0);

            double result = GeometryUtils.GetTriangleArea(point1, point2, point3);

            Assert.Equal(result, Convert.ToDouble(50.0));
            Assert.True(GeometryUtils.IsTriangle(point1, point2, point3));

        }

        [Fact]
        public void Point_Inside_Triangle_Test()
        {
            int days = 23;
            Coordinates point1 = GeometryUtils.GetPosition(GeometryUtils.GetAngle(5, days), 1000);
            Coordinates point2 = GeometryUtils.GetPosition(GeometryUtils.GetAngle(-1, days), 500);
            Coordinates point3 = GeometryUtils.GetPosition(GeometryUtils.GetAngle(-3, days), 2000);
            Coordinates s = new Coordinates();
            Assert.True(GeometryUtils.PointIsInsideOfTriangle(point1, point2, point3, s));
        }
    }
}
