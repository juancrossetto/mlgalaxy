﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Test.Builders;
using System.Collections.Generic;
using Xunit;

namespace Galaxy.API.Test.Test.Domain
{
    public class SolarSystemTest
    {
      
        [Fact]
        public void Has_Three_Planets_Test()
        {
            int expectedPlanets = 3;
            Planet P1 = new PlanetBuilder().WithFerengiValues().Build();
            Planet P2 = new PlanetBuilder().WithBetasoideValues().Build();
            Planet P3 = new PlanetBuilder().WithVulcanoValues().Build();
            SolarSystem SS = new SolarSystemBuilder()
                                            .WithThreePlanets(P1, P2, P3).Build();

            Assert.Equal(SS.Planets.Count, expectedPlanets);
        }


    }
}
