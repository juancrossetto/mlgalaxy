﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Test.Builders
{
    public class SolarSystemBuilder
    {
        private SolarSystem _ss = new SolarSystem();

        public SolarSystemBuilder WithName(string name)
        {
            _ss.Name = name;
            return this;
        }

        public SolarSystemBuilder WithThreePlanets(Planet planet1, Planet planet2, Planet planet3)
        {
            _ss.Planets.Add(planet1);
            _ss.Planets.Add(planet2);
            _ss.Planets.Add(planet3);
            return this;
        }

        public SolarSystem Build()
        {
            return this._ss;
        }

    }
}
