﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Test.Builders
{
    public class PlanetBuilder
    {
        private Planet _planet = new Planet();

        public PlanetBuilder WithName(string name)
        {
            _planet.Name = name;
            return this;
        }

        public PlanetBuilder WithDistance(int distance)
        {
            _planet.Distance = distance;
            return this;
        }

        public PlanetBuilder WithSpeed(double speed)
        {
            _planet.Speed = speed;
            return this;
        }
        public PlanetBuilder WithSolarSystem(SolarSystem ss)
        {
            _planet.SolarSystem = ss;
            return this;
        }

        public PlanetBuilder WithNoSolarSystem()
        {
            _planet.SolarSystem = new SolarSystem();
            return this;
        }

        public PlanetBuilder WithFerengiValues()
        {
            _planet = new Planet
            {
                Name = "Ferengi",
                Distance = 500,
                Speed = -1
            };
            return this;
        }


        public PlanetBuilder WithBetasoideValues()
        {
            _planet = new Planet
            {
                Name = "Betasoide",
                Distance = 2000,
                Speed = -3
            };
            return this;
        }
        public PlanetBuilder WithVulcanoValues()
        {
            _planet = new Planet
            {
                Name = "Vulcano",
                Distance = 1000,
                Speed = 5
            };
            return this;
        }

        public Planet Build()
        {
            return this._planet;
        }

    }
}
