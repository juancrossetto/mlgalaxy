﻿using Galaxy.API.Core.Process;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galaxy.API.Process.Scheduler
{
    public class HangfireJobScheduler
    {
        public static void ScheduleRecurringJobs()
        {
            int hour = 2;
            int minutes = 0;
            RecurringJob.RemoveIfExists(nameof(ForecastJob));
            RecurringJob.AddOrUpdate<ForecastJob>(nameof(ForecastJob),
                job => job.Run(JobCancellationToken.Null),
                Cron.Daily(hour, minutes), TimeZoneInfo.Utc);
        }
    }
}
