﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Domain.Models.Enums;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Process
{
    public class ForecastJob : IForecastJob
    {
        private readonly ILogger<IForecastJob> _logger;
        private readonly IPlanetService _planetService;
        private readonly ISolarSystemService _solarSystemService;
        private readonly IForecastService _forecastService;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public ForecastJob(ILogger<IForecastJob> logger, IWeatherService weatherService, 
                           IConfiguration configuration, IPlanetService planetService,
                           ISolarSystemService solarSystemService, IForecastService forecastService,
                           IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _planetService = planetService;
            _solarSystemService = solarSystemService;
            _forecastService = forecastService;
            _mapper = mapper;
        }

        public async Task<ForecastReport> Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            var result = await RunProcess(0);
            return result;
            
        }

        public async Task<ForecastReport> RunProcess(int quantityOfDays)
        {
            
            _logger.LogInformation($"Forecast Job Starts at {DateTime.Now}");
            var forecastReport = InitProcess(quantityOfDays);
            _logger.LogInformation($"Forecast Job Completed at {DateTime.Now}");
            return forecastReport;
        }

        #region Private Properties
        
        private ForecastReport InitProcess(int quantityOfDays)
        {
            ForecastReport result = null;
            try
            {
                int period = 0;
                if (quantityOfDays <= 0)
                {
                    int yearsToProcess = _configuration.GetValue<int>("ApplicationConfig:YearsToProcess");
                    int daysPerYear = _configuration.GetValue<int>("ApplicationConfig:DaysPerYear");
                    period = yearsToProcess * daysPerYear;
                }
                else
                    period = quantityOfDays;
            

                //Reset forecast
                this.CleanForecast();

                Planet Ferengi = this._planetService.GetPlanet("Ferengi").Result.Planet; 
                Planet Betasoide = this._planetService.GetPlanet("Betasoide").Result.Planet;
                Planet Vulcano = this._planetService.GetPlanet("Vulcano").Result.Planet;
                SolarSystem SolarSystem = this._solarSystemService.GetSolarSystem("MELI").Result.SolarSystem;
                Forecast reportForecast;
                var forecastResponse = this._forecastService.CalculateForecast(period, SolarSystem);
                if (!string.IsNullOrEmpty(forecastResponse.Message))
                {
                    _logger.LogInformation(forecastResponse.Message);
                }
                else
                {
                    reportForecast = forecastResponse.Forecast;
                    //Persist forecast 
                    this.PersistForeCast(reportForecast);

                    _logger.LogInformation($"The forecast report has {period} days");
                    //Log info about forecast days
                    result = ForecastUtils.GetForecastReport(reportForecast);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error ocurred in the process. Error: {ex.Message}");
                _logger.LogInformation($"An error ocurred in the process. Error: {ex.Message}");
            }
            return result;
        }
        
        private async void PersistForeCast(Forecast forecast)
        {
            try
            {
                if(forecast == null)
                {
                    _logger.LogWarning("The forecast to save is null");
                } else
                {
                    var result = await _forecastService.CreateForecast(forecast);
                    if (!result.Success)
                        _logger.LogInformation($"An error ocurred when trying to the forecast. Error: {result.Message}");
                    else
                        _logger.LogInformation($"Forecast was persisted");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"An error ocurred when trying to persist the forecast. Error: {ex.Message}");
            }
        }

        private async void CleanForecast()
        {
            try
            {
                var result = await this._forecastService.DeleteForecast();
                _logger.LogInformation(result.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error ocurred when trying to reset the forecast. Error: {ex.Message}");
            }
        }

      
        
        #endregion
    }
}
