﻿using Galaxy.API.Core.Domain.Entities;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Process
{
    public interface IForecastJob
    {
        /// <summary>
        /// Run the Forecast process
        /// </summary>
        /// <returns>String with the report</returns>
        Task<ForecastReport> RunProcess(int quantityOfDays);
    }
}
