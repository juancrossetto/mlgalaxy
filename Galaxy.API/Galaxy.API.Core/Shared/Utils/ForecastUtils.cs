﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Domain.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galaxy.API.Core.Shared.Utils
{
    public static class ForecastUtils
    {
        public static ForecastReport GetForecastReport(Forecast report)
        {
            int DroughtDays = report.Weathers.Where(x => x.State.Equals(WeatherState.DG)).Count();
            int OptimalConditionsDays = report.Weathers.Where(x => x.State.Equals(WeatherState.OC)).Count();
            int RainyDays = report.Weathers.Where(x => x.State.Equals(WeatherState.RN)).Count();
            int NormalDays = report.Weathers.Where(x => x.State.Equals(WeatherState.NM)).Count();

            var result = new ForecastReport{
                                            SolarSystemName = report.SolarSystem.Name,
                                            NormalDays = NormalDays,
                                            DroughtDays = DroughtDays,
                                            OptimalConditionsDays = OptimalConditionsDays,
                                            RainyDays = RainyDays,
                                            DayWithMaxRainIntensity = report.DayWithMaxRainIntensity};
            //var result = $"Report Forecast for {report.SolarSystem.Name} Solar System:." +
            //              $" Normal days: {NormalDays}, " +
            //              $" Drought days: {DroughtDays}, " +
            //              $" Optimal Conditions days: {OptimalConditionsDays}, " +
            //              $" Rainy days: {RainyDays}, " +
            //              $" Day with the maximum peak of rain: {report.DayWithMaxRainIntensity}";
            return result;
        }
    }
}
