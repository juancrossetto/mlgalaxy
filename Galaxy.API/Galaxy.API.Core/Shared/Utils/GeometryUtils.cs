﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Galaxy.API.Core.Shared.Utils
{
    public static class GeometryUtils
    {
        /// <summary>
        /// Get the angle
        /// </summary>
        /// <param name="speed">speed</param>
        /// <param name="day">day</param>
        /// <returns>Return the angle</returns>
        public static double GetAngle(double speed, int day) 
            => speed * day;


        /// <summary>
        /// Get the angle in grades and convert it in radians 
        /// </summary>
        /// <param name="angle">angle</param>
        /// <returns>Angle converted in radians</returns>
        public static double GetRadianFromAngle(double angle)
            => Math.PI * angle / 180.0;
        

        /// <summary>
        /// Get the Area of the triangle using the Area formula
        /// Area formula: 0.5 * [x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)];
        /// </summary>
        /// <param name="ppA">Point(coordinates) A</param>
        /// <param name="ppB">Point(coordinates) B</param>
        /// <param name="ppC">Point(coordinates) C</param>
        /// <returns>The Area of the triangle formed by the three points</returns>
        public static double GetTriangleArea(Coordinates ppA, Coordinates ppB, Coordinates ppC)
        {
            
            
            if (ppA == null || ppB == null || ppC == null)
                throw new CoordinatesNullException("Some of the coordinates are null");

            double area = Math.Abs(((ppA.X * (ppB.Y - ppC.Y)) +
                                   (ppB.X * (ppC.Y - ppA.Y)) +
                                   (ppC.X * (ppA.Y - ppB.Y))) / 2.0);

            return area;
        }

        /// <summary>
        /// Method to know if the position of the three points  form a triangle
        /// If the perimeter result is 0, it mean that the points are aligned.
        /// </summary>
        /// <param name="ppA">Point(coordinates) A</param>
        /// <param name="ppB">Point(coordinates) B</param>
        /// <param name="ppC">Point(coordinates) C</param>
        /// <returns>Return true if the coordinates form a triangle</returns>
        public static bool IsTriangle(Coordinates ppA, Coordinates ppB, Coordinates ppC)
        {
            if (ppA == null || ppB == null || ppC == null)
                throw new CoordinatesNullException("Some of the coordinates are null");

            double triangleArea = GetTriangleArea(ppA, ppB, ppC);
            return !triangleArea.Equals(0.0d);
        }        

        /// <summary>
        /// The distance formula is square root of ((x2 - x1)^2  - (y2 - y1)^2)
        /// </summary>
        /// <param name="p1">Coordinates of point 1</param>
        /// <param name="p2">Coordinates of point 2</param>
        /// <returns>The distance between two points</returns>
        public static double GetDistanceBwPoints(Coordinates p1, Coordinates p2)
        {
            var distance = Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
            return distance;
        }

        /// <summary>
        /// Get the perimeter of the triangle 
        /// </summary>
        /// <param name="a">Point(coordinates)  A</param>
        /// <param name="b">Point(coordinates)  B</param>
        /// <param name="c">Point(coordinates)  C</param>
        /// <returns>The perimeter of the triangle formed by the three points</returns>
        public static double GetTrianglePerimeter(Coordinates a, Coordinates b, Coordinates c)
        {
            if (a == null || b == null || c == null)
                throw new CoordinatesNullException("Some of the coordinates are null");

            double distanceFromAtoB = GetDistanceBwPoints(a, b);
            double distanceFromBtoC = GetDistanceBwPoints(b, c);
            double distanceFromCtoA = GetDistanceBwPoints(c, a);
            return distanceFromAtoB + distanceFromBtoC + distanceFromCtoA;

        }


        /// <summary>
        ///  A function to check whether point P(x, y) lies inside the triangle formed by A(x1, y1), (x2, y2) and C(x3, y3)
        /// <param name="a">Coordinates a</param>
        /// <param name="b">Coordinates b</param>
        /// <param name="c">Coordinates c</param>
        /// <param name="p">Point</param>
        /// <returns>Return true if point is inside of the triangle</returns>
        public static bool PointIsInsideOfTriangle(Coordinates a, Coordinates b, Coordinates c, Coordinates p)
        {
            if (a == null || b == null || c == null || p == null)
                throw new CoordinatesNullException("Some of the coordinates are null");

            /* Calculate area of triangle ABC */
            double A = GetTriangleArea(a, b, c);

            /* Calculate area of triangle PBC */
            double A1 = GetTriangleArea(p, b, c);

            /* Calculate area of triangle PAC */
            double A2 = GetTriangleArea(a, p, c);

            /* Calculate area of triangle PAB */
            double A3 = GetTriangleArea(a, b, p);

            /* Check if sum of A1, A2 and A3 is same as A */
            return A == (A1 + A2 + A3);
        }

        /// <summary>
        /// Calculate if three points are aligned/colinear, 
        /// </summary>
        /// <param name="p1">Coordinates of Point one</param>
        /// <param name="p2">Coordinates of Point two</param>
        /// <param name="p3">Coordinates of Point three</param>
        /// <returns>Return true if all points are aligned</returns>
        public static bool PointsAligned(Coordinates p1, Coordinates p2, Coordinates p3)
        {
            if (p1 == null || p2 == null || p3 == null)
                throw new CoordinatesNullException("Some of the coordinates are null");

            double value = (p2.Y - p1.Y) * (p3.X - p2.X) -
                           (p2.X - p1.X) * (p3.Y - p2.Y);

            return (value == 0.0);
        }

        /// <summary>
        /// Get form position
        /// </summary>
        /// <param name="angle">angle</param>
        /// <param name="distance">distance</param>
        /// <returns>Return position of the form</returns>
        public static Coordinates  GetPosition(double angle, int distance)
        {
            double radians = GetRadianFromAngle(angle);
            double x = Math.Round(Math.Cos(radians) * distance, 8);
            double y = Math.Round(Math.Sin(radians) * distance, 8);
            return new Coordinates(x, y);
        }

        /// <summary>
        /// Get angles in degrees of the planet parameter
        /// </summary>
        /// <param name="day">day</param>
        /// <param name="speed">speed</param>
        /// <returns>The angles of the planet position</returns>
        public static double GetAngleInDegrees(int day, double speed)
        {
            double angle = GeometryUtils.GetAngle(speed, day) % 360;
            if (speed < 0)
                angle = 360 + angle;

            return angle;
        }

    }
}
