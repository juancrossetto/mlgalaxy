﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
using System.Security.Claims;

namespace Galaxy.API.Core.Shared.Utils
{
    public class HangfireDashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            var httpContext = context.GetHttpContext();
            var userRole = httpContext.User.FindFirst(ClaimTypes.Role)?.Value;
            return true;
        }
    }
}
