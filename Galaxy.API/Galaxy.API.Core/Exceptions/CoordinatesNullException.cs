﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galaxy.API.Core.Exceptions
{
    [Serializable]
    public class CoordinatesNullException : Exception
    {
        public CoordinatesNullException()
        {

        }

        public CoordinatesNullException(string name)
            : base(String.Format("Coordinates of the planet are null: {0}", name))
        {

        }

    }
}
