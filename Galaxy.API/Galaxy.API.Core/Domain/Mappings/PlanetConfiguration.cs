﻿using Galaxy.API.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Galaxy.API.Core.Domain.Mappings
{
    public class PlanetConfiguration : IEntityTypeConfiguration<Planet>
    {

        public void Configure(EntityTypeBuilder<Planet> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name);
            builder.Property(x => x.Speed);
            builder.Property(x => x.Distance);
        }
    }
}
