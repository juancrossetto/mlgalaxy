﻿using Galaxy.API.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Galaxy.API.Core.Domain.Mappings
{
    public class ForecastConfiguration : IEntityTypeConfiguration<Forecast>
    {

        public void Configure(EntityTypeBuilder<Forecast> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.DayWithMaxRainIntensity);
            builder.Property(x => x.Period);
            
        }
    }
}
