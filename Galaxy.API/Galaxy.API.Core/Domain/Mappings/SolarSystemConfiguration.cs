﻿using Galaxy.API.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Galaxy.API.Core.Domain.Mappings
{
    public class SolarSystemConfiguration : IEntityTypeConfiguration<SolarSystem>
    {

        public void Configure(EntityTypeBuilder<SolarSystem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name);
            builder.HasMany(x => x.Planets).WithOne(b => b.SolarSystem).HasForeignKey(b => b.SolarSystemId);
            builder.HasOne(x => x.Forecast).WithOne(b => b.SolarSystem).HasForeignKey<Forecast>(b => b.Id);
        }
    }
}
