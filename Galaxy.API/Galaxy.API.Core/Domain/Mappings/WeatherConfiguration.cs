﻿using Galaxy.API.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Galaxy.API.Core.Domain.Mappings
{
    public class WeatherConfiguration : IEntityTypeConfiguration<Weather>
    {

        public void Configure(EntityTypeBuilder<Weather> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Day);
            builder.Property(x => x.State);
        }
    }
}
