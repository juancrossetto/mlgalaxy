﻿using System.ComponentModel;

namespace Galaxy.API.Domain.Models.Enums
{
    public enum WeatherState : byte
    {
        /// <summary>
        /// Planets form a triangle but the sun is outside of this.
        /// </summary>
        [Description("Normal")]
        NM = 1,

        /// <summary>
        /// Planets are aligned respect to the sun
        /// </summary>
        [Description("Drought")]
        DG = 2,


        /// <summary>
        /// Planets are aligned between their but these aren't aligned respect to the sun
        /// </summary>
        [Description("Rainy")]
        RN = 3,

        /// <summary>
        /// Planets form a triangle and the sun is inside of this.
        /// </summary>
        [Description("OptimalConditions")]
        OC = 4
    }
}
