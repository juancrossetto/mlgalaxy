﻿using Galaxy.API.Core.Shared.Utils;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.API.Core.Domain.Entities
{
    /// <summary>
    /// Represent a Planet of the Solar system
    /// </summary>
    [Table("Planets")]
    public class Planet : BaseEntity<int>
    {
        /// <summary>
        /// Planet name
        /// </summary>
        ///[BsonElement("PlanetName")]
        public string Name { get; set; }

        /// <summary>
        /// Angular Speed (Degrees per day)
        /// </summary>
        public double Speed { get; set; }


        /// <summary>
        /// Distance from the sun in kilometers
        /// </summary>
        public int Distance { get; set; }

        [ForeignKey("Id")]
        public int SolarSystemId { get; set; }

        /// <summary>
        /// Solar system of the planet
        /// </summary>
        public SolarSystem SolarSystem
        {
            get => _lazyLoader.Load(this, ref _solarSystem);
            set => _solarSystem = value;
        }


        /// <summary>
        /// Get the position of the planet parameter
        /// </summary>
        /// <param name="day">day</param>
        /// <returns>The position of the planet</returns>
        public Coordinates GetPosition(int day)
        {
            var angle = GeometryUtils.GetAngleInDegrees(day, this.Speed);
            Coordinates coordinates = GeometryUtils.GetPosition(angle, this.Distance);
            return coordinates;
        }




        #region Private Properties
        private readonly ILazyLoader _lazyLoader;
        private SolarSystem _solarSystem;
        #endregion


        #region Constructors
        /// <summary>
        /// Empty constructor 
        /// </summary>
        public Planet()
        {

        }

        public Planet(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        /// <summary>
        /// Constructor with properties
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="speed">speed</param>
        /// <param name="distance">distance</param>
        /// <param name="solarSystem">solarSystem</param>
        public Planet(string name, double speed, int distance, SolarSystem solarSystem)
        {
            Name = name;
            Speed = speed;
            Distance = distance;
            SolarSystem = solarSystem;
        }
        #endregion
    }
}
