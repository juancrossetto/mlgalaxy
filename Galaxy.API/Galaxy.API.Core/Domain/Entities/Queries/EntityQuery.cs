﻿namespace Galaxy.API.Core.Domain.Entities.Queries
{
    public class EntityQuery : Query
    {

        public EntityQuery(int page, int itemsPerPage) : base(page, itemsPerPage)
        {
        }
    }
}
