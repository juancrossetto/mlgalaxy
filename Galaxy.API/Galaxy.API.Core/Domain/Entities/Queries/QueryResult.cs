﻿using System.Collections.Generic;

namespace Galaxy.API.Core.Domain.Entities.Queries
{
    public class QueryResult<T>
    {
        public List<T> Items { get; set; } = new List<T>();
        public int TotalItems { get; set; } = 0;
    }
}
