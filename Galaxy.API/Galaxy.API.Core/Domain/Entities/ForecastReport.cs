﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Galaxy.API.Core.Domain.Entities
{
    public class ForecastReport
    {
        public string SolarSystemName { get; set; }
        public int NormalDays { get; set; }
        public int OptimalConditionsDays { get; set; }
        public int DroughtDays { get; set; }
        public int RainyDays { get; set; }
        public int DayWithMaxRainIntensity { get; set; }
    }
}
