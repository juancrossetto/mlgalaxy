﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.API.Core.Domain.Entities
{
    public abstract class BaseEntity<TId>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("Id")]
        public TId Id { get; set; }

        protected BaseEntity(TId id)
        {
            Id = Id;
        }

        // EF requires an empty constructor
        protected BaseEntity()
        {
        }
    }
}
