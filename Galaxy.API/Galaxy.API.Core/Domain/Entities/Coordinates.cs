﻿namespace Galaxy.API.Core.Domain.Entities
{
    /// <summary>
    /// Represent the position in the Cartesian system
    /// </summary>
    public class Coordinates
    {
        /// <summary>
        /// Position of X in the Cartesian system
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Position of Y in the Cartesian system
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">x point</param>
        /// <param name="y">y point</param>
        public Coordinates(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Coordinates()
        {
            this.X = 0.0d;
            this.Y = 0.0d;
        }
    }
}
