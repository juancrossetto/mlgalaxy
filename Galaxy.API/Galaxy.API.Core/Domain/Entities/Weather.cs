﻿using Galaxy.API.Domain.Models.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.API.Core.Domain.Entities
{
    /// <summary>
    /// Represent a weather day.
    /// </summary>
    public class Weather : BaseEntity<int>
    {
        /// <summary>
        /// Day
        /// </summary>
        public int Day { get; set; }

        /// <summary>
        /// Weather State
        /// </summary>
        public WeatherState State { get; set; }

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public Weather()
        {
        }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="day">day</param>
        /// <param name="state">state</param>
        public Weather(int day, WeatherState state)
        {
            this.Day = day;
            this.State = state;
        }

        #endregion  

    }
}
