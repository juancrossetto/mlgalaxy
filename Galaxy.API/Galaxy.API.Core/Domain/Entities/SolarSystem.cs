﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.API.Core.Domain.Entities
{
    /// <summary>
    /// Solar System with three planets
    /// </summary>
    public class SolarSystem : BaseEntity<int>
    {

        /// <summary>
        /// Name of the Solar System
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Planets
        /// </summary>
        public ICollection<Planet> Planets
        {
            get => _lazyLoader.Load(this, ref _planets);
            set => _planets = value;
        }

        /// <summary>
        /// Weathers
        /// </summary>
        public Forecast Forecast
        {
            get => _lazyLoader.Load(this, ref _forecast);
            set => _forecast = value;
        }


        #region Private Properties
        private readonly ILazyLoader _lazyLoader;
        private ICollection<Planet> _planets;
        private Forecast _forecast;
        #endregion

        #region Constructors
        public SolarSystem(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        public SolarSystem()
        {
            Planets = new List<Planet>() {
                Capacity = 2
            };
            Forecast = new Forecast();
        }

        public SolarSystem(string name)
        {
            Name = name;
        }
        #endregion
    }
}
