﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.API.Core.Domain.Entities
{
    /// <summary>
    /// Represent the extended Weather Report
    /// </summary>
    [Table("Forecast")]
    public class Forecast : BaseEntity<int>
    {
        /// <summary>
        /// Day when rained with max intensity
        /// </summary>
        public int DayWithMaxRainIntensity { get; set; }

        public ICollection<Weather> Weathers { get; set; }

        /// <summary>
        /// Period in Days of forecast
        /// </summary>
        public int Period { get; set; }

        public SolarSystem SolarSystem { get; set; }


        #region Constructors
        /// <summary>
        /// Empty Constructor
        /// </summary>
        public Forecast()
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dayWithMaxRainIntensity"> dayWithMaxRainIntensity</param>
        /// <param name="period">period in days</param>
        /// <param name="weathers">weathers</param>
        /// <param name="solarSystem">solar System</param>
        public Forecast(int dayWithMaxRainIntensity, int period, List<Weather> weathers, SolarSystem solarSystem)
        {
            DayWithMaxRainIntensity = dayWithMaxRainIntensity;
            Period = period;

            if (weathers != null)
                Weathers = weathers;
            else
                weathers = new List<Weather>();

            SolarSystem = solarSystem;
        }
        #endregion
    }
}
