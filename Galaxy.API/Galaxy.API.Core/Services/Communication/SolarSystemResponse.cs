﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Domain.Services.Communication
{
    public class SolarSystemResponse : BaseResponse
    {
        public SolarSystem SolarSystem { get; private set; }

        private SolarSystemResponse(bool success, string message, SolarSystem solarSystem) : base(success, message)
        {
            SolarSystem = solarSystem;
        }

        public SolarSystemResponse(SolarSystem solarSystem) : this(true, string.Empty, solarSystem) { }

        public SolarSystemResponse(string message, bool success = false) : this(success, message, null) { }
    }
}
