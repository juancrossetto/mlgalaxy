﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Domain.Services.Communication;

namespace Galaxy.API.Core.Services.Communication
{
    public class ForecastResponse : BaseResponse
    {
        public Forecast Forecast { get; private set; }

        private ForecastResponse(bool success, string message, Forecast forecast) : base(success, message)
        {
            Forecast = forecast;
        }

        public ForecastResponse(Forecast forecast) : this(true, string.Empty, forecast) { }

        public ForecastResponse(string message, bool success = false) : this(success, message, null) { }
    }
}
