﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Domain.Services.Communication
{
    public class WeatherResponse : BaseResponse
    {
        public Weather Weather { get; private set; }

        private WeatherResponse(bool success, string message, Weather weather) : base(success, message)
        {
            Weather = weather;
        }

        public WeatherResponse(Weather weather) : this(true, string.Empty, weather) { }

        public WeatherResponse(string message, bool success = false) : this(success, message, null) { }
    }
}
