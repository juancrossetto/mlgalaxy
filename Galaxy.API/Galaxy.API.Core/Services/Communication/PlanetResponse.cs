﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Domain.Services.Communication
{
    public class PlanetResponse : BaseResponse
    {
        public Planet Planet { get; private set; }

        private PlanetResponse(bool success, string message, Planet planet) : base(success, message)
        {
            Planet = planet;
        }

        public PlanetResponse(Planet planet) : this(true, string.Empty, planet) { }

        public PlanetResponse(string message, bool success = false) : this(success, message, null) { }
    }
}
