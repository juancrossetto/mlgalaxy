﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Domain.Services.Communication;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Services
{
    /// <summary>
    /// Service of the Solar System Entity with its own methods
    /// </summary>
    public interface ISolarSystemService
    {

        /// <summary>
        /// Get all Solar Systems
        /// </summary>
        /// <returns>Return a list with all Solar Systems persisted</returns>
        Task<IEnumerable<SolarSystem>> GetAllSolarSystems();

        /// <summary>
        /// Get a Solar System by name
        /// </summary>
        /// <param name="name">Name of the Solar System</param>
        /// <returns>Return the Solar System</returns>
        Task<SolarSystemResponse> GetSolarSystem(string name);

        /// <summary>
        /// Create a Solar System
        /// </summary>
        /// <param name="weather">Solar System to create</param>
        /// <returns>Return the Solar System created</returns>
        Task<SolarSystemResponse> CreateSolarSystem(SolarSystem solarSystem);

        /// <summary>
        /// Delete a Solar System by name
        /// </summary>
        /// <param name="name">Name of the Solar System to delete</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task<SolarSystemResponse> DeleteSolarSystem(string name);

        /// <summary>
        /// Update a Solar System
        /// </summary>
        /// <param name="solarSystem">Solar System to create</param>
        /// <returns>Return the Solar System updated</returns>
        SolarSystemResponse UpdateSolarSystem(SolarSystem solarSystem);
    }
}
