﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Domain.Services.Communication;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Services
{
    /// <summary>
    /// Service of the Planet Entity with its own methods
    /// </summary>
    public interface IPlanetService
    {

        /// <summary>
        /// Get all planets
        /// </summary>
        /// <returns>Return a list with all planets persisted</returns>
        Task<IEnumerable<Planet>> GetAllPlanets();

        /// <summary>
        /// Get a Planet
        /// </summary>
        /// <param name="name">Name of the Planet</param>
        /// <returns>Return the Planet</returns>
        Task<PlanetResponse> GetPlanet(string name);

        /// <summary>
        /// Create a Planet
        /// </summary>
        /// <param name="weather">Planet to create</param>
        /// <returns>Return the Planet created</returns>
        Task<PlanetResponse> CreatePlanet(Planet planet);

        /// <summary>
        /// Delete a Planet by name
        /// </summary>
        /// <param name="name">Name of the Planet to delete</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task<PlanetResponse> DeletePlanet(string name);

        /// <summary>
        /// Update a Planet
        /// </summary>
        /// <param name="planet">Planet to create</param>
        /// <returns>Return the Planet updated</returns>
        PlanetResponse UpdatePlanet(Planet planet);
    }
}
