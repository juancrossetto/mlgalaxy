﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services.Communication;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Services
{
    /// <summary>
    /// Service of the Forecast Entity with its own methods
    /// </summary>
    public interface IForecastService
    {
        /// <summary>
        /// Persist a forecast
        /// </summary>
        /// <param name="forecast">Forecast to persist</param>
        /// <returns>Return the forecast persisted</returns>
        Task<ForecastResponse> CreateForecast(Forecast forecast);

        /// <summary>
        /// Delete all Forecast (should be one)
        /// </summary>
        /// <returns>Return a message if the action was successfully</returns>
        Task<ForecastResponse> DeleteForecast();

        /// <summary>
        /// Calculate the forecast of some solar system in "x" period
        /// </summary>
        /// <param name="period">period to calculate</param>
        /// <param name="ss">solar system</param>
        /// <returns>Return a forecast report</returns>
        ForecastResponse CalculateForecast(int period, SolarSystem ss);

    }
}