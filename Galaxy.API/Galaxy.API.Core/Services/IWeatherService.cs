﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Entities.Queries;
using Galaxy.API.Domain.Services.Communication;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Core.Services
{
    /// <summary>
    /// Service of the Weather Entity with its own methods
    /// </summary>
    public interface IWeatherService
    {
        /// <summary>
        /// Create a weather
        /// </summary>
        /// <param name="weather">weather to create</param>
        /// <returns>Return the weather created</returns>
        Task<WeatherResponse> CreateWeather(Weather weather);

        /// <summary>
        /// Bulk insert of weathers
        /// </summary>
        /// <param name="weathers">weathers to insert</param>
        /// <param name="quantityInsertPerLot">Quantity of weathers to persist per lot(so as not to persist all together, we divide by lots</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task<WeatherResponse> BulkInsertWeather(List<Weather> weathers, int quantityInsertPerLot);

        /// <summary>
        /// Delete a Weather day
        /// </summary>
        /// <param name="day">day of the weather to delete</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task<WeatherResponse> DeleteWeather(int day);

        /// <summary>
        /// Bulk Delete of all Weathers
        /// </summary>
        /// <returns>Return a message if the action was successfully</returns>
        Task<WeatherResponse> DeleteAllWeathers();

        /// <summary>
        /// Get all Weathers with pagination
        /// </summary>
        /// <param name="query">page and items per page</param>
        /// <returns>Return a list of weather with pagination</returns>
        Task<QueryResult<Weather>> ListAsync(EntityQuery query);

        /// <summary>
        /// Get a weather
        /// </summary>
        /// <param name="day">day of the weather</param>
        /// <returns>Return the weather</returns>
        Task<WeatherResponse> GetWeather(int day);

        /// <summary>
        /// Get all weathers persisted
        /// </summary>
        /// <returns>Return a list with all weathers persisted</returns>
        Task<IEnumerable<Weather>> GetAllWeathers();

        /// <summary>
        /// Get Weathers of a range of days.
        /// </summary>
        /// <param name="quantityOfDays">quantity of days we want to get info</param>
        /// <returns>return a range of weathers</returns>
        Task<IEnumerable<Weather>> GetWeatherRange(int quantityOfDays);
    }
}
