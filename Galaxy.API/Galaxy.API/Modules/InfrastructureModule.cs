﻿using Autofac;
using Galaxy.API.Core.Process;
using Galaxy.API.Core.Services;
using Galaxy.API.Infrastructure.Data.Repositories;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Galaxy.API.Infrastructure.ServicesImpl;

namespace Galaxy.API.Modules
{
    public class InfrastructureModule : Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PlanetRepository>().As<IPlanetRepository>().InstancePerLifetimeScope();

            builder.RegisterType<SolarSystemRepository>().As<ISolarSystemRepository>().InstancePerLifetimeScope();

            builder.RegisterType<WeatherRepository>().As<IWeatherRepository>().InstancePerLifetimeScope();

            builder.RegisterType<ForecastRepository>().As<IForecastRepository>().InstancePerLifetimeScope();

            builder.RegisterType<PlanetService>().As<IPlanetService>().InstancePerLifetimeScope();

            builder.RegisterType<WeatherService>().As<IWeatherService>().InstancePerLifetimeScope();

            builder.RegisterType<SolarSystemService>().As<ISolarSystemService>().InstancePerLifetimeScope();

            builder.RegisterType<ForecastService>().As<IForecastService>().InstancePerLifetimeScope();

            builder.RegisterType<ForecastJob>().As<IForecastJob>().InstancePerLifetimeScope();


            builder.RegisterGeneric(typeof(IBaseRepository<,>))
                .As(typeof(IBaseRepository<,>))
                .InstancePerLifetimeScope();


        }
    }
}
