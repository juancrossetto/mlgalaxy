﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Entities.Queries;
using Galaxy.API.Infrastructure.Extensions;
using Galaxy.API.Services.DTOs;

namespace Galaxy.API.Mappers
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles()
        {
            CreateMap<Planet, PlanetDTO>().ReverseMap();
            CreateMap<Weather, WeatherDTO>()
                .ForMember(src => src.State,
                           opt => opt.MapFrom(src => src.State.ToDescriptionString())).ReverseMap();
            CreateMap<SolarSystem, SolarSystemDTO>().ReverseMap();

            CreateMap<QueryResult<Weather>, QueryResultDTO<WeatherDTO>>().ReverseMap();

            CreateMap<EntityQuery, WeathersQueryDTO>().ReverseMap();
            CreateMap<ForecastReport, ForecastReportDTO>().ReverseMap();
            
        }
    }
}
