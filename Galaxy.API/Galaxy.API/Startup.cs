﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Infrastructure;
using Galaxy.API.Infrastructure.Data.Repositories;
using Galaxy.API.Modules;
using Galaxy.API.Process.Scheduler;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Galaxy.API
{
    public class Startup
    {
        public Microsoft.Extensions.Configuration.IConfiguration Configuration { get; }

        public Startup(Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddResponseCaching();

            //To test services
            services.AddSwaggerGen(cfg =>
            {
                cfg.SwaggerDoc("v1", new Info
                {
                    Title = "Galaxy API",
                    Version = "v1.1",
                    Description = "Galaxy RESTful API built with ASP.NET Core 2.2 and Database In Memory",
                    Contact = new Contact
                    {
                        Name = "Juan Manuel Vazquez Crossetto",
                        Url = "https://bitbucket.org/juancrossetto/mlgalaxy/src/master/",
                    },
                    License = new Swashbuckle.AspNetCore.Swagger.License
                    {
                        Name = "MIT",
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                cfg.IncludeXmlComments(xmlPath);
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.Remove(services.First(x => x.ServiceType == typeof(IAntiforgery)));
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseInMemoryDatabase("MELIDB");
            });

            services.AddAutoMapper();

            var container = new ContainerBuilder();
            container.Populate(services);
            container.RegisterModule(new InfrastructureModule());


            // Add the processing server as IHostedService
            //services.AddHangfireServer();


            services.AddHangfire(config =>
            {
                GlobalConfiguration.Configuration.UseMemoryStorage();
            });

            return BuildDependencyInjectionProvider(services);
         
        }


        private static IServiceProvider BuildDependencyInjectionProvider(IServiceCollection services)
        {
            var builder = new ContainerBuilder();

            // Populate the container using the service collection
            builder.Populate(services);

            Assembly webAssembly = Assembly.GetExecutingAssembly();
            Assembly coreAssembly = Assembly.GetAssembly(typeof(BaseEntity<>));
            Assembly infrastructureAssembly = Assembly.GetAssembly(typeof(BaseRepository<,>));
            builder.RegisterAssemblyTypes(webAssembly, coreAssembly, infrastructureAssembly).AsImplementedInterfaces();

            Autofac.IContainer applicationContainer = builder.Build();
            return new AutofacServiceProvider(applicationContainer);
        }

        public void Configure(IApplicationBuilder app,
                          ILoggerFactory loggerFactory, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();


            //-------Config Swagger to show an interface to test rest services-------
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Galaxy API");
            });
            //-------End Swagger config-------.
            
       
            loggerFactory.AddFile("Logs/GalaxyLog-{Date}.txt");

            //-------Config to Hangfire for process in background-------

            app.UseHangfireDashboard("/process", new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardAuthorizationFilter() },
                IgnoreAntiforgeryToken = true

            });
            
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                WorkerCount = 1
            });
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });
            HangfireJobScheduler.ScheduleRecurringJobs();
            //-------End config HangFire-------

            app.UseMvc();

        }
    }
}
