﻿namespace Galaxy.API.Services.DTOs
{
    public class QueryDTO
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
