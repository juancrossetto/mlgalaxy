﻿namespace Galaxy.API.Services.DTOs
{
    public class PlanetDTO
    {
        public string Name { get; set; }
        public double Speed { get; set; }
        public int Distance { get; set; }
        
        public SolarSystemDTO SolarSystem { get; set; }
    }
}
