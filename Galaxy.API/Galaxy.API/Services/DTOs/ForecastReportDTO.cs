﻿using System.Runtime.Serialization;

namespace Galaxy.API.Services.DTOs
{
    [DataContract]
    public class ForecastReportDTO
    {
        [DataMember(Name ="Solar System")]
        public string SolarSystemName { get; set; }

        [DataMember(Name = "Normal days")]
        public int NormalDays { get; set; }

        [DataMember(Name = "Optimal conditions days")]
        public int OptimalConditionsDays { get; set; }

        [DataMember(Name = "Drought days")]
        public int DroughtDays { get; set; }

        [DataMember(Name = "Rainy days")]
        public int RainyDays { get; set; }

        [DataMember(Name = "Day with max rain intensity")]
        public int DayWithMaxRainIntensity { get; set; }
    }
}
