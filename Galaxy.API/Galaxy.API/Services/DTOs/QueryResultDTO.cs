﻿using System.Collections.Generic;

namespace Galaxy.API.Services.DTOs
{
    public class QueryResultDTO<T>
    {
        public int TotalItems { get; set; } = 0;
        public List<T> Items { get; set; } = new List<T>();
    }
}
