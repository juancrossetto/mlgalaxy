﻿using System.Collections.Generic;

namespace Galaxy.API.Services.DTOs
{
    public class ErrorDTO
    {
        public bool Success => false;
        public List<string> Messages { get; private set; }

        public ErrorDTO(List<string> messages)
        {
            this.Messages = messages ?? new List<string>();
        }

        public ErrorDTO(string message)
        {
            this.Messages = new List<string>();

            if (!string.IsNullOrWhiteSpace(message))
            {
                this.Messages.Add(message);
            }
        }
    }
}
