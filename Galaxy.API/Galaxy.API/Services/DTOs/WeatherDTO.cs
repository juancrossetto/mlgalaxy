﻿using System.ComponentModel.DataAnnotations;

namespace Galaxy.API.Services.DTOs
{
    public class WeatherDTO
    {
        [Required]
        public int Day { get; set; }

        [Required]
        public string State { get; set; }
    }
}
