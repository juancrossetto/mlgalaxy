﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Galaxy.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class PlanetController : Controller
    {
        private readonly IPlanetService _planetService;
        private readonly IMapper _mapper;

        public PlanetController(IPlanetService planetService, IMapper mapper)
        {
            _planetService = planetService;
            _mapper = mapper;
        }


        // GET: api/planet/name=1
        /// <summary>
        /// Get Planet by name
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>Planet</returns>
        [ProducesResponseType(typeof(PlanetDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpGet("{name}")]
        public async Task<IActionResult> GetPlanet(string name)
        {
            var result = await _planetService.GetPlanet(name);
            if (!result.Success)
            {
                return BadRequest(new ErrorDTO(result.Message));
            }
            var planet = _mapper.Map<Planet, PlanetDTO>(result.Planet);
            return Ok(planet);
        }

        /// <summary>
        /// Create a Planet
        /// </summary>
        /// <param name="planetDTO">Planet to create</param>
        /// <returns>Return the planet created</returns>
        [HttpPost]
        [ProducesResponseType(typeof(PlanetDTO), 201)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        public async Task<IActionResult> PostAsync([FromBody] PlanetDTO planetDTO)
        {
            var planet = _mapper.Map<PlanetDTO, Planet>(planetDTO);
            var result = await _planetService.CreatePlanet(planet);

            if (!result.Success)
            {
                return BadRequest(new ErrorDTO(result.Message));
            }

            var planetResult = _mapper.Map<Planet, PlanetDTO>(result.Planet);
            return Ok(planetResult);
        }

        /// <summary>
        /// Update a planet
        /// </summary>
        /// <param name="planetDTO">Planet to update</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        [HttpPut]
        public IActionResult Put([FromBody]PlanetDTO planetDTO)
        {
            var planet = _mapper.Map<PlanetDTO, Planet>(planetDTO);
            var result = this._planetService.UpdatePlanet(planet);
            if (!result.Success)
                return BadRequest(new ErrorDTO(result.Message));
            else
            {
                var planetResult = _mapper.Map<Planet, PlanetDTO>(result.Planet);
                return Ok(planetResult);
            }
        }

        /// <summary>
        /// Delete a planet
        /// </summary>
        /// <returns>Return a message if the action was successfully or not</returns>
        [HttpDelete("{name}")]
        [ProducesResponseType(typeof(PlanetDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        public async Task<IActionResult> DeleteAsync(string name)
        {
            var result = await _planetService.DeletePlanet(name);

            if (!result.Success)
                return BadRequest(new ErrorDTO(result.Message));
            else
                return Ok(result.Message);
        }
    }
}