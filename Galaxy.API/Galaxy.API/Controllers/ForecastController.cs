﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Process;
using Galaxy.API.Core.Services;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Services.DTOs;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Galaxy.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ForecastController : Controller
    {
        private readonly IForecastJob _forecastJob;
        private readonly ISolarSystemService _solarSystemService;
        private readonly IForecastService _forecastService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public ForecastController(IForecastService forecastService, ISolarSystemService solarSystemService,
                                  IForecastJob forecastJob, IMapper mapper, IConfiguration configuration)
        {
            _solarSystemService = solarSystemService;
            _forecastService = forecastService;
            _forecastJob = forecastJob;
            _mapper = mapper;
            _configuration = configuration;
        }

        /// <summary>
        /// Generate a Forecast Report
        /// </summary>
        /// <param name="days">Quantity of days to process</param>
        /// <param name="nameSS">Name of the Solar System</param>
        /// <returns>Generate a Forecast Report</returns>
        [ProducesResponseType(typeof(string), 201)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpGet("GenerateForecast")]
        public IActionResult GenerateForecast(int days, string nameSS)
        {
            int period = 0;
            if (days <= 0)
            {
                int yearsToProcess = _configuration.GetValue<int>("ApplicationConfig:YearsToProcess");
                int daysPerYear = _configuration.GetValue<int>("ApplicationConfig:DaysPerYear");
                period = yearsToProcess * daysPerYear;
            }
            else
                period = days;
            ForecastReportDTO result;
            ForecastReport forecastReport;
            try
            {
                var ssResult = this._solarSystemService.GetSolarSystem(nameSS).Result.SolarSystem;
                if(ssResult == null || string.IsNullOrEmpty(ssResult.Name))
                    return BadRequest(new ErrorDTO($"Error in process. Doesn't exist a solar system with name {nameSS}"));
                else
                {

                    var forecast = _forecastService.CalculateForecast(period, ssResult).Forecast;
                    forecastReport = ForecastUtils.GetForecastReport(forecast);
                    result = _mapper.Map<ForecastReport, ForecastReportDTO>(forecastReport);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorDTO($"Error in process. Error: {ex}"));
            }
            return Ok(result);
        }

        /// <summary>
        /// Generate a Forecast Report background
        /// </summary>
        /// <param name="days">Quantity of days to process</param>
        /// <returns>Generate a Forecast Report</returns>
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpGet("GenerateBackgroundForecast")]
        public IActionResult GenerateBackgroundForecast(int days)
        {
            string forecastReport = string.Empty;
            try
            {
                BackgroundJob.Enqueue(() => _forecastJob.RunProcess(days));
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorDTO($"Error in process. Error: {ex}"));
            }
            return Ok($"Forecast generated successfully, the report is on the process page.");
        }
        

    }
}