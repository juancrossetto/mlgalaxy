﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Entities.Queries;
using Galaxy.API.Core.Services;
using Galaxy.API.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class WeatherController : Controller
    {
        private readonly IWeatherService _weatherService;
        private readonly IMapper _mapper;
        public WeatherController(IWeatherService weatherService, IMapper mapper)
        {
            _weatherService = weatherService;
            _mapper = mapper;
        }

        /// <summary>
        /// Create a weather
        /// </summary>
        /// <returns>Return the weather created</returns>
        // POST api/weather
        [ProducesResponseType(typeof(WeatherDTO), 201)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpPost("Weather")]
        public async Task<IActionResult> PostAsync([FromBody] WeatherDTO weatherDTO)
        {
            try
            {
                var weather = _mapper.Map<WeatherDTO, Weather>(weatherDTO);
                var result = await _weatherService.CreateWeather(weather);

                if (!result.Success)
                {
                    return BadRequest(new ErrorDTO(result.Message));
                }

                var weatherResult = _mapper.Map<Weather, WeatherDTO>(result.Weather);
                return Ok(weatherResult);
            }
            catch (AutoMapperMappingException)
            {
                return BadRequest("Se indicó un estado de clima no válido");
            }
            catch (Exception ex)
            {
                return BadRequest("Error: " + ex);
            }
           
        }

        /// <summary>
        /// Delete weather
        /// </summary>
        /// <returns>Return a message if the action was successfully or not</returns>
        [HttpDelete("{day}")]
        [ProducesResponseType(typeof(WeatherDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> DeleteAsync(int day)
        {
            var result = await _weatherService.DeleteWeather(day);

            if (!result.Success)
                return BadRequest(new ErrorDTO(result.Message));
            else
                return Ok(result.Message);
        }

        /// <summary>
        /// Delete all Weathers
        /// </summary>
        /// <returns>Return a message if the action was successfully or not</returns>
        [ProducesResponseType(typeof(WeatherDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpDelete("DeleteWeathers")]
        public async Task<IActionResult> DeletAll()
        {
            var result = await _weatherService.DeleteAllWeathers();
            
            
            return Ok("Weathers Deleted successfully");
        }



        /// <summary>
        /// Get all Weathers with Pagination.
        /// </summary>
        /// <returns>List of weathers.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(QueryResultDTO<WeatherDTO>), 200)]
        public async Task<QueryResultDTO<WeatherDTO>> ListAsync([FromQuery] WeathersQueryDTO query)
        {
            var weathersQuery = _mapper.Map<WeathersQueryDTO, EntityQuery>(query);
            var queryResult = await _weatherService.ListAsync(weathersQuery);

            var resource = _mapper.Map<QueryResult<Weather>, QueryResultDTO<WeatherDTO>>(queryResult);
            return resource;
        }

        // GET: api/weather/?day=1
        /// <summary>
        /// Get Weather by day
        /// </summary>
        /// <param name="day"></param>
        /// <returns>Weather of the day parameter</returns>
        [ProducesResponseType(typeof(WeatherDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [HttpGet("{day}")]
        public async Task<IActionResult> GetWeather(int day)
        {
            var result = await _weatherService.GetWeather(day);
            if (!result.Success)
            {
                return BadRequest(new ErrorDTO(result.Message));
            }
            var weather = _mapper.Map<Weather, WeatherDTO>(result.Weather);
            return Ok(weather);
        }

        // GET: api/weather/weatherrange?quantityOfDays=10
        /// <summary>
        /// Get extended forecast
        /// </summary>
        /// <param name="quantityOfDays">Quantity of days to the forecast</param>
        /// <returns>Extended forecast</returns>
        [HttpGet("ForecastRange/{quantityOfDays}")]
        [ProducesResponseType(typeof(IEnumerable<WeatherDTO>), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetWeatherRange(int quantityOfDays)
        {
            var weathers = await _weatherService.GetWeatherRange(quantityOfDays);
            if (weathers == null)
            {
                return BadRequest(new ErrorDTO("An error ocurred when trying to get the forecast"));
            }

            var result = _mapper
                .Map<IEnumerable<Weather>, IEnumerable<WeatherDTO>>(weathers);
            
            return Ok(result);
        }

        
        
    }
}