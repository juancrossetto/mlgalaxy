﻿using AutoMapper;
using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Galaxy.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class SolarSystemController : Controller
    {
        private readonly ISolarSystemService _solarSystemService;
        private readonly IMapper _mapper;

        public SolarSystemController(ISolarSystemService solarSystemService, IMapper mapper)
        {
            _solarSystemService = solarSystemService;
            _mapper = mapper;
        }

        /// <summary>
        /// Create a Solar system
        /// </summary>
        /// <param name="solarSystemDTO">Solar system to create</param>
        /// <returns>Return the solar system created</returns>
        [HttpPost]
        [ProducesResponseType(typeof(SolarSystemDTO), 201)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        public async Task<IActionResult> PostAsync([FromBody] SolarSystemDTO solarSystemDTO)
        {
            var solarSystem = _mapper.Map<SolarSystemDTO, SolarSystem>(solarSystemDTO);
            var result = await _solarSystemService.CreateSolarSystem(solarSystem);

            if (!result.Success)
            {
                return BadRequest(new ErrorDTO(result.Message));
            }

            var solarSystemResult = _mapper.Map<SolarSystem, SolarSystemDTO>(result.SolarSystem);
            return Ok(solarSystemResult);
        }

        /// <summary>
        /// Update solar system
        /// </summary>
        /// <param name="solarSystemDTO">Solar system to update</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        [HttpPut]
        public IActionResult Put([FromBody]SolarSystemDTO solarSystemDTO)
        {
            var solarSystem =  _mapper.Map<SolarSystemDTO, SolarSystem>(solarSystemDTO);
            
            var result =  this._solarSystemService.UpdateSolarSystem(solarSystem);
            if (!result.Success)
                return BadRequest(new ErrorDTO(result.Message));
            else
            {
                var ssResult = _mapper.Map<SolarSystem, SolarSystemDTO>(result.SolarSystem);
                return Ok(ssResult);
            }
        }

        /// <summary>
        /// Delete solar system
        /// </summary>
        /// <returns>Return a message if the action was successfully or not</returns>
        [HttpDelete("{name}")]
        [ProducesResponseType(typeof(SolarSystemDTO), 200)]
        [ProducesResponseType(typeof(ErrorDTO), 400)]
        public async Task<IActionResult> DeleteAsync(string name)
        {
            var result = await _solarSystemService.DeleteSolarSystem(name);

            if (!result.Success)
                return BadRequest(new ErrorDTO(result.Message));
            else
                return Ok(result.Message);
        }
    }
}