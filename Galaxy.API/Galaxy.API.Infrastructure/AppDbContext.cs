﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Mappings;
using Microsoft.EntityFrameworkCore;

namespace Galaxy.API.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {
        }
        public DbSet<Planet> Planets { get; set; }
        public DbSet<SolarSystem> SolarSystems { get; set; }
        public DbSet<Weather> Weathers { get; set; }
        public DbSet<Forecast> Forecasts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PlanetConfiguration());
            builder.ApplyConfiguration(new SolarSystemConfiguration());
            builder.ApplyConfiguration(new WeatherConfiguration());
            builder.ApplyConfiguration(new ForecastConfiguration());

            base.OnModelCreating(builder);
        }

    }
}
