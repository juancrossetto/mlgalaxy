﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Domain.Services.Communication;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.ServicesImpl
{
    public class SolarSystemService : ISolarSystemService
    {
        private readonly ISolarSystemRepository _solarSystemRepository;

        public SolarSystemService(ISolarSystemRepository solarSystemRepository)         
           => _solarSystemRepository = solarSystemRepository;
        


        public async Task<IEnumerable<SolarSystem>> GetAllSolarSystems()
        {
            var solarSystems = await _solarSystemRepository.GetAll();
            return solarSystems;
        }

        
        public async Task<SolarSystemResponse> GetSolarSystem(string name)
        {
            try
            {
                var solarSystem = await _solarSystemRepository.GetByName(name) ?? new SolarSystem();
                if (solarSystem == null)
                    return new SolarSystemResponse("SolarSystem not found.");
                else
                    return new SolarSystemResponse(solarSystem);
            }
            catch (Exception ex)
            {
                return new SolarSystemResponse($"An error occurred when trying to get the solarSystem: {ex.Message}");
            }

        }

        public async Task<SolarSystemResponse> CreateSolarSystem(SolarSystem solarSystem)
        {
            try
            {
                await _solarSystemRepository.Create(solarSystem);

                return new SolarSystemResponse(solarSystem);
            }
            catch (Exception ex)
            {
                return new SolarSystemResponse($"An error occurred when saving the solarSystem: {ex.Message}");
            }
        }

        public async Task<SolarSystemResponse> DeleteSolarSystem(string name)
        {
            try
            {
                var solarSystem = await _solarSystemRepository.GetByName(name);
                if(solarSystem != null)
                {
                    var solarSystemDeleted = _solarSystemRepository.Delete(solarSystem);
                    if (!solarSystemDeleted.IsCompleted)
                        return new SolarSystemResponse("Error deleting Solar System.");
                    else
                        return new SolarSystemResponse($"Solar System {name} was deleted successfully", true);
                } else
                    return new SolarSystemResponse($"Doens't exist Solar System with the name {name}");

            }
            catch (Exception ex)
            {
                return new SolarSystemResponse($"An error occurred when deleting the Solar System: {ex.Message}");
            }
        }


        public SolarSystemResponse UpdateSolarSystem(SolarSystem solarSystem)
        {
            try
            {
                var solarSystemUpdated = _solarSystemRepository.Update(solarSystem);
                if (!solarSystemUpdated.IsCompleted)
                    return new SolarSystemResponse("Error updating SolarSystem.");

                return new SolarSystemResponse(solarSystem);
            }
            catch (Exception ex)
            {
                return new SolarSystemResponse($"An error occurred when updating the solarSystem: {ex.Message}");
            }
        }


    }
}
