﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Entities.Queries;
using Galaxy.API.Core.Services;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Domain.Services.Communication;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.ServicesImpl
{
    public class WeatherService : IWeatherService
    {
        private readonly IWeatherRepository _weatherRepository;
        private readonly IMemoryCache _cache;

        public WeatherService(IWeatherRepository weatherRepository, IMemoryCache cache)
        {
            _weatherRepository = weatherRepository;
            _cache = cache;
        }

        //Use cache when get forecast(by page and quantity of elements)
        public async Task<QueryResult<Weather>> ListAsync(EntityQuery query)
        {
            string cacheKey = GetCacheKeyForWeathersQuery(query);

            var weathers = await _cache.GetOrCreateAsync(cacheKey, (entry) => {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);
                return _weatherRepository.GetAllWithPagination(query);
            });

            return weathers;
        }
        
        public async Task<WeatherResponse> GetWeather(int day)
        {
            try
            {
                var weather = await _weatherRepository.GetWeatherByday(day);
                if (weather == null)
                    return new WeatherResponse("Weather not found.");
                else
                    return new WeatherResponse(weather);
            }
            catch (Exception ex)
            {
                return new WeatherResponse($"An error occurred when trying to get the weather: {ex.Message}");
            }
 
        }

        public async Task<IEnumerable<Weather>> GetWeatherRange(int quantityOfDays)
        {
            var weatherRange = await _weatherRepository.GetWeatherRange(quantityOfDays);
            return weatherRange;
        }

        public async Task<IEnumerable<Weather>> GetAllWeathers()
        {
            var weathers = await _weatherRepository.GetAll();
            return weathers;
        }

        public async Task<WeatherResponse> CreateWeather(Weather weather)
        {
            try
            {
                await _weatherRepository.Create(weather);

                return new WeatherResponse(weather);
            }
            catch (Exception ex)
            {
                return new WeatherResponse($"An error occurred when saving the weather: {ex.Message}");
            }
        }


        public async Task<WeatherResponse> BulkInsertWeather(List<Weather> weathers, int quantityInsertPerLot)
        {
            try
            {
                int maxQuantityToInsert = quantityInsertPerLot;
                int reminder = weathers.Count % maxQuantityToInsert;
                int lots = weathers.Count / maxQuantityToInsert;
               

                for (int i = 0; i < lots; i++)
                {
                    await _weatherRepository.BulkInsert(weathers.GetRange(i * maxQuantityToInsert, maxQuantityToInsert));
                }

                //If there are reminder, insert it.
                if(reminder > 1)
                    await _weatherRepository.BulkInsert(weathers.GetRange(weathers.Count - reminder, reminder));

                return new WeatherResponse("Forecast Inserted successfully", true);
            }
            catch (Exception ex)
            {
                return new WeatherResponse($"An error occurred when saving the forecast: {ex.Message}");
            }
        }

        public async Task<WeatherResponse> DeleteWeather(int day)
        {
            try
            {
                var existingWeather = await _weatherRepository.GetWeatherByday(day);

                if (existingWeather != null)
                {
                    var weatherDeleted = _weatherRepository.Delete(existingWeather);
                    if (!weatherDeleted.IsCompleted)
                        return new WeatherResponse("Error deleting Weather.");
                    else
                        return new WeatherResponse($"Weather of the day {day} was deleted successfully", true);
                }
                else
                    return new WeatherResponse($"Doens't exist a Weather of the day {day}");

            }
            catch (Exception ex)
            {
                return new WeatherResponse($"An error occurred when deleting the weather: {ex.Message}");
            }
        }

        public async Task<WeatherResponse> DeleteAllWeathers()
        {
            try
            {
                await _weatherRepository.DeleteAll();
                return new WeatherResponse("All Weathers were deleted successfully", true);
            }
            catch (Exception ex)
            {
                return new WeatherResponse($"An error occurred when deleting all weathers: {ex.Message}");
            }
        }

        private string GetCacheKeyForWeathersQuery(EntityQuery query)
        {
            string key = CacheKeys.WeatherList.ToString();
            
            key = string.Concat(key, "_", query.Page, "_", query.ItemsPerPage);
            return key;
        }
    }
}
