﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Core.Services.Communication;
using Galaxy.API.Core.Shared.Utils;
using Galaxy.API.Domain.Models.Enums;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.ServicesImpl
{
    public class ForecastService : IForecastService
    {
        private readonly IForecastRepository _forecastRepository;

        public ForecastService(IForecastRepository forecastRepository)
        =>    _forecastRepository = forecastRepository;


        public async Task<ForecastResponse> CreateForecast(Forecast forecast)
        {
            try
            {
                await this._forecastRepository.Create(forecast);

                return new ForecastResponse(forecast);
            }
            catch (Exception ex)
            {
                return new ForecastResponse($"An error occurred when saving the forecast: {ex.Message}");
            }
        }

        public async Task<ForecastResponse> DeleteForecast()
        {
            try
            {
                await _forecastRepository.DeleteAll();
                return new ForecastResponse($"Forecast was deleted successfully", true);

            }
            catch (Exception ex)
            {
                return new ForecastResponse($"An error occurred when deleting the forecast: {ex.Message}");
            }
        }

        public ForecastResponse CalculateForecast(int period, SolarSystem ss)
        {
            try
            {
                List<Weather> weathers = new List<Weather>();
                double maxPerimeterOfTriangle = 0;
                int dayWithMaxRainIntensity = 0;
                Planet firstPlanet = ss.Planets.ElementAt(0);
                Planet secondPlanet = ss.Planets.ElementAt(1);
                Planet thirdPlanet = ss.Planets.ElementAt(2);
                for (int day = 1; day <= period; day++)
                {
                    Weather weather = new Weather(day, WeatherState.NM);

                    Coordinates sunPosition = new Coordinates();
                    Coordinates positionFirstPlanet = firstPlanet.GetPosition(day);
                    Coordinates positionSecondPlanet = secondPlanet.GetPosition(day);
                    Coordinates positionThirdPlanet = thirdPlanet.GetPosition(day);
                    if (GeometryUtils.PointsAligned(positionFirstPlanet, positionSecondPlanet, positionThirdPlanet))
                    {
                        //Now we have to know if the sun is aligned respect to the planets
                        //If the planets are aligned, we take two planets and the sun and compare.
                        if (GeometryUtils.PointsAligned(positionFirstPlanet, positionSecondPlanet, sunPosition))
                            weather.State = WeatherState.DG;
                        else
                            weather.State = WeatherState.OC;
                    }
                    else if (GeometryUtils.IsTriangle(positionFirstPlanet, positionSecondPlanet, positionThirdPlanet))
                    {
                        //Now we have to know if the sun is inside or outside the triangle. 
                        if (GeometryUtils.PointIsInsideOfTriangle(positionFirstPlanet, positionSecondPlanet, positionThirdPlanet, sunPosition))
                        {
                            weather.State = WeatherState.RN;
                            double perimeterBwPlanetsOfToday = GeometryUtils.GetTrianglePerimeter(positionFirstPlanet, positionSecondPlanet, positionThirdPlanet);
                            if (perimeterBwPlanetsOfToday > maxPerimeterOfTriangle)
                            {
                                maxPerimeterOfTriangle = perimeterBwPlanetsOfToday;
                                dayWithMaxRainIntensity = day;
                            }

                        }
                    }
                    weathers.Add(weather);
                }

                Forecast forecast = new Forecast(dayWithMaxRainIntensity, period, weathers, ss);
                return new ForecastResponse(forecast);
            }
            catch (Exception ex)
            {
                return new ForecastResponse($"An error occurred when trying to calculate the forecast: {ex.Message}");
            }
            
        }
    }
}
