﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Services;
using Galaxy.API.Domain.Services.Communication;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.ServicesImpl
{
    public class PlanetService : IPlanetService
    {
        private readonly IPlanetRepository _planetRepository;

        public PlanetService(IPlanetRepository planetRepository) =>
            _planetRepository = planetRepository;
        


        public async Task<IEnumerable<Planet>> GetAllPlanets()
        {
            var planets = await _planetRepository.GetAll();
            return planets;
        }

        
        public async Task<PlanetResponse> GetPlanet(string name)
        {
            try
            {
                var planet = await _planetRepository.GetByName(name);
                if (planet == null)
                    return new PlanetResponse("Planet not found.");
                else
                    return new PlanetResponse(planet);
            }
            catch (Exception ex)
            {
                return new PlanetResponse($"An error occurred when trying to get the planet: {ex.Message}");
            }
         
        }

        public async Task<PlanetResponse> CreatePlanet(Planet planet)
        {
            try
            {

                await _planetRepository.Create(planet);
                return new PlanetResponse(planet);
            }
            catch (Exception ex)
            {
                return new PlanetResponse($"An error occurred when saving the planet: {ex.Message}");
            }
        }

        public async Task<PlanetResponse> DeletePlanet(string name)
        {
            try
            {
                var planet = await _planetRepository.GetByName(name);
                if (planet != null)
                {
                    var planetDeleted = _planetRepository.Delete(planet);
                    if (!planetDeleted.IsCompleted)
                        return new PlanetResponse("Error deleting the Planet.");
                    else
                        return new PlanetResponse($"Planet {name} was deleted successfully", true);
                }
                else
                    return new PlanetResponse($"Doens't exist a Planet with the name {name}");

            }
            catch (Exception ex)
            {
                return new PlanetResponse($"An error occurred when deleting the Planet: {ex.Message}");
            }
        }


        public PlanetResponse UpdatePlanet(Planet planet)
        {
            try
            {
                var planetUpdated = _planetRepository.Update(planet);
                if (!planetUpdated.IsCompleted)
                    return new PlanetResponse("Error updating Planet.");

                return new PlanetResponse(planet);
            }
            catch (Exception ex)
            {
                return new PlanetResponse($"An error occurred when updating the planet: {ex.Message}");
            }
        }


    }
}
