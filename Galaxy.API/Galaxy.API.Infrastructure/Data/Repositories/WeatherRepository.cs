﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories
{
    public class WeatherRepository : BaseRepository<Weather, int>, IWeatherRepository
    {
        public WeatherRepository(AppDbContext db) : base(db) { }


        public async Task<Weather> GetWeatherByday(int day)
        {
            return await _db.Weathers
                 .AsNoTracking()
                 .FirstOrDefaultAsync(e => e.Day.Equals(day));
        }
       
        public async Task<IEnumerable<Weather>> GetWeatherRange(int quantityOfDays)
        {
            return await _db.Weathers.Where(x => x.Day <= quantityOfDays).ToListAsync();
        }

        public async Task RemoveWeatherByDay(int day)
        {
            Weather entity = await this.GetWeatherByday(day);
            _db.Weathers.Remove(entity);
            await _db.SaveChangesAsync();
        }
        

    }
}
