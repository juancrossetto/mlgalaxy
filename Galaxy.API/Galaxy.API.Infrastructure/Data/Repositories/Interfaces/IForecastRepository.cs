﻿using Galaxy.API.Core.Domain.Entities;

namespace Galaxy.API.Infrastructure.Data.Repositories.Interfaces
{
    /// <summary>
    /// Repository of the Forecast Entity with its own methods
    /// </summary>
    public interface IForecastRepository : IBaseRepository<Forecast, int>
    {

    }
}
