﻿using Galaxy.API.Core.Domain.Entities.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories.Interfaces
{
    /// <summary>
    /// Base Repository shared by all entities with common methods
    /// </summary>
    public interface IBaseRepository<TEntity, TId> where TEntity : class
    {
        /// <summary>
        /// Get a list of some entity
        /// </summary>
        /// <param name="query">page and items per page</param>
        /// <returns>Return a list of the entity with pagination by page and items per page</returns>
        Task<QueryResult<TEntity>> GetAllWithPagination(EntityQuery query);

        /// <summary>
        /// Get all elements persisted of some entity
        /// </summary>
        /// <returns>Return a list with all elements of some entity</returns>
        Task<IEnumerable<TEntity>> GetAll();

        /// <summary>
        /// Get some entity by id
        /// </summary>
        /// <param name="id">identifier if the entity</param>
        /// <returns>Return some entity</returns>
        Task<TEntity> GetById(TId id);

        /// <summary>
        /// Create an entity
        /// </summary>
        /// <param name="entity">entity to create</param>
        /// <returns>Return the entity created</returns>
        Task Create(TEntity entity);

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity">entity to update</param>
        /// <returns>Return the entity updated</returns>
        Task Update(TEntity entity);

        /// <summary>
        /// Delete an entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task Delete(TId id);

        /// <summary>
        /// delete an entity by itself
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task Delete(TEntity entity);

        /// <summary>
        /// Delete of elements persisted of some entity
        /// </summary>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task DeleteAll();

        /// <summary>
        /// Bulk insert of some entity
        /// </summary>
        /// <param name="entities">entities to persist</param>
        /// <returns>Return a message if the action was successfully or not</returns>
        Task BulkInsert(List<TEntity> entities);
    }
}
