﻿using Galaxy.API.Core.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories.Interfaces
{
    /// <summary>
    /// Repository of the Weather Entity with its own methods
    /// </summary>
    public interface IWeatherRepository : IBaseRepository<Weather, int>
    {
        /// <summary>
        /// Delete a weather by day
        /// </summary>
        /// <param name="day">Day to delete</param>
        /// <returns>Return a message if the action was successfully</returns>
        Task RemoveWeatherByDay(int day);

        /// <summary>
        /// Get a weather by day
        /// </summary>
        /// <param name="day">Day of the weather to get</param>
        /// <returns>Return a weather</returns>
        Task<Weather> GetWeatherByday(int day);

        /// <summary>
        /// Get a range of weathers
        /// </summary>
        /// <param name="quantityOfDays">Quantity of days to get, starting with day 1</param>
        /// <returns>Return a list of wheaters inside of this range</returns>
        Task<IEnumerable<Weather>> GetWeatherRange(int quantityOfDays);
    }
}
