﻿using Galaxy.API.Core.Domain.Entities;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories.Interfaces
{
    /// <summary>
    /// Repository of the Solar System Entity with its own methods
    /// </summary>
    public interface ISolarSystemRepository : IBaseRepository<SolarSystem, int>
    {
        /// <summary>
        /// Get a Solar System By Name
        /// </summary>
        /// <param name="name">Name of the solar system</param>
        /// <returns>Return a solar system</returns>
        Task<SolarSystem> GetByName(string name);
    }
}
