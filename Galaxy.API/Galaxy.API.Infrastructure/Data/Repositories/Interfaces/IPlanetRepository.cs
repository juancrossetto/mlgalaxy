﻿using Galaxy.API.Core.Domain.Entities;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories.Interfaces
{
    /// <summary>
    /// Repository of the Planet Entity with its own methods
    /// </summary>
    public interface IPlanetRepository : IBaseRepository<Planet, int>
    {
        /// <summary>
        /// Get a planet by name
        /// </summary>
        /// <param name="name">Name of the planet</param>
        /// <returns>Return a planet</returns>
        Task<Planet> GetByName(string name);

    }
}
