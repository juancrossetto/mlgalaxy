﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Infrastructure;
using Galaxy.API.Infrastructure.Data.Repositories;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories
{
    public class SolarSystemRepository : BaseRepository<SolarSystem, int>, ISolarSystemRepository
    {

        public SolarSystemRepository(AppDbContext db) : base(db) { }

        public async Task<SolarSystem> GetByName(string name)
        {
            return await _db.SolarSystems
                 .FirstOrDefaultAsync(p => p.Name.ToUpper().Equals(name.ToUpper()));
        }
    }
}
