﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;

namespace Galaxy.API.Infrastructure.Data.Repositories
{
    public class ForecastRepository : BaseRepository<Forecast, int>, IForecastRepository
    {
        public ForecastRepository(AppDbContext db) : base(db) { }
    
    }
}
