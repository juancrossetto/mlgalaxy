﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories
{ 
    public class PlanetRepository : BaseRepository<Planet, int>, IPlanetRepository
    {
        public PlanetRepository(AppDbContext db) : base(db) { }

        public async Task<Planet> GetByName(string name)
        {
            return await _db.Planets.FirstOrDefaultAsync(p => p.Name.ToUpper().Equals(name.ToUpper()));
        }

    }
}
