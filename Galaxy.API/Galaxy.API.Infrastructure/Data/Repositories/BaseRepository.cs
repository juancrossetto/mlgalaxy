﻿using Galaxy.API.Core.Domain.Entities;
using Galaxy.API.Core.Domain.Entities.Queries;
using Galaxy.API.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galaxy.API.Infrastructure.Data.Repositories
{
    public class BaseRepository<TEntity, TId> : IBaseRepository<TEntity, TId> where TEntity : BaseEntity<TId>
    {
        protected static string CacheKey = typeof(TEntity).Name.ToString();
        protected readonly AppDbContext _db;
        public BaseRepository(AppDbContext db) => _db = db;


        public async Task<QueryResult<TEntity>> GetAllWithPagination(EntityQuery query)
        {
            var resultAsync = await this.GetAll();
            var queryable = resultAsync.AsQueryable().AsNoTracking();
            List<TEntity> results = queryable.Skip((query.Page - query.ItemsPerPage) * query.ItemsPerPage)
                                             .Take(query.ItemsPerPage)
                                             .ToList();
            
            return new QueryResult<TEntity>
            {
                Items = results,
                TotalItems = queryable.Count(),
            };
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
           return await _db.Set<TEntity>().ToListAsync();
        }


        public async Task<TEntity> GetById(TId id)
        {
            return await _db.Set<TEntity>()
                 .AsNoTracking()
                 .FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task Create(TEntity entity)
        {
            await _db.Set<TEntity>().AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task BulkInsert(List<TEntity> entities)
        {
            await _db.Set<TEntity>().AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task Update(TEntity entity)
        {
            _db.Set<TEntity>().Update(entity);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(TId id)
        {
            var entity = await this.GetById(id);
            _db.Set<TEntity>().Remove(entity);
            await _db.SaveChangesAsync();
        }
        public async Task Delete(TEntity entity)
        {
            _db.Set<TEntity>().Remove(entity);
            await _db.SaveChangesAsync();
        }
        public async Task DeleteAll()
        {
            _db.Set<TEntity>().RemoveRange(_db.Set<TEntity>());
            await _db.SaveChangesAsync();
        }

    }
}
