﻿using Galaxy.API.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Galaxy.API.Infrastructure.Data
{
    /// <summary>
    /// Class that generates information in the database in memory, to start the app with loaded information.
    /// </summary>
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new AppDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<AppDbContext>>()))
            {
                if (context.Planets.Any())
                    return;   // Data was already seeded

                SolarSystem ss = new SolarSystem("MELI");
                Planet planet1 = new Planet("Ferengi", 1 , 500, ss);
                Planet planet2 = new Planet("Betasoide", 3, 2000, ss);
                Planet planet3 = new Planet("Vulcano", -5, 1000, ss);
                context.SolarSystems.Add(ss);
                context.Planets.Add(planet1);
                context.Planets.Add(planet2);
                context.Planets.Add(planet3);

                context.SaveChanges();
            }
        }
    }
}
